import yaml
import sys
import os
import shutil
from contextlib import contextmanager
import subprocess
import shlex
import shutil


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def pullSources(config):
    ci_job_token = os.getenv("CI_JOB_TOKEN")
    for name, source in config['sources'].items():
        # Skip over source if it is the current project
        if name == os.getenv("CI_PROJECT_NAME"):
            continue
        if 'gitlab' in source['url'] and 'https' in source['url']:
            url = source['url'][:8]
            url += "gitlab-ci-token:{}@".format(ci_job_token)
            url += source['url'][8:]
        else:
            url = source['url']
        cmd = f"ipbb add git {url}"
        if 'tag' in source:
            cmd += f" -b {source['tag']} --depth 1"
        elif 'branch' in source:
            cmd += f" -b {source['branch']} --depth 1"
        elif 'commit' in source:
            cmd += f" -r {source['commit']}"
        if os.path.isdir(f"./{name}") is False or ("override" in source and source["override"] is True):
            try:
                shutil.rmtree(f"./{name}", ignore_errors=True)
            except FileNotFoundError as e:
                pass
            subprocess.call(cmd, shell=True)
            if os.path.isfile(f"./{name}/dependencies.yml"):
                print(f"Repository {name} has dependencies file - recursively pulling dependencies...")
                with open(f"./{name}/dependencies.yml") as stream:
                    sub_config = yaml.safe_load(stream)
                    pullSources(sub_config)
        else:
            print(f"Skipping repository {name} - already exists")


def buildWorkArea(config):
    if os.path.exists("ci/builds/work"):
        print("Project area already exists. Removing src and proj directories...")
        shutil.rmtree("ci/builds/work/src", ignore_errors=True)
        shutil.rmtree("ci/builds/work/proj", ignore_errors=True)
        os.makedirs("ci/builds/work/src")
        os.makedirs("ci/builds/work/proj")
    else:
        print("Creating new project area...")
        with cd("ci/builds"):
            subprocess.call(["ipbb", "init", "work"])
    with cd("ci/builds/work/src"):
        print("Pulling sources...")
        pullSources(config)
        subprocess.call([
            "rsync", "-ar", "{}".format(os.getenv("CI_PROJECT_DIR")), "./",
            "--exclude", "ci"
        ])


def main():
    if len(sys.argv) > 1:
        config_path = sys.argv[1]
    else:
        config_path = "ci/config.yml"
    with open(config_path) as stream:
        config = yaml.safe_load(stream)
    buildWorkArea(config)


if __name__ == '__main__':
    main()
