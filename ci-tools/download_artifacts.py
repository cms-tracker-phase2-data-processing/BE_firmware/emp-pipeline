from minio import Minio
import yaml
import sys
import subprocess
import os


def main():
    client = Minio(
        endpoint=os.getenv("ARTIFACTS_HOST"),
        access_key=os.getenv("ARTIFACTS_ACCESS_KEY"),
        secret_key=os.getenv("ARTIFACTS_SECRET_KEY"),
        secure=os.getenv("ARTIFACTS_SECURE") == "TRUE"
    )
    bucket = os.getenv("ARTIFACTS_BUCKET")
    archive = "{}-{}.tar.gz".format(
        os.getenv("CI_PIPELINE_ID"),
        os.getenv("PREVIOUS_JOB").replace(" ", "_")
    )
    client.fget_object(bucket, archive, archive)
    subprocess.call("tar xf {}".format(archive), shell=True)


if __name__ == '__main__':
    main()
