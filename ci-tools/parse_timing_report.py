import re
import pandas as pd
import sys
import os

pd.options.display.width = 0


class TimingPath:
    def __init__(self, lines):
        self.lines = lines
        self.slackns = float(re.search("-[0-9\.]*", self.lines[0]).group())
        self.source = re.search("(?<=Source:)(.*)", self.lines[1]).group().strip()
        self.sourceclk = self._getClock(self.lines[2])
        self.destination = re.search("(?<=Destination:)\ *(.*)", self.lines[3]).group().strip()
        self.destinationclk = self._getClock(self.lines[4])
        
    def _getClock(self, line):
        try:
            return re.search("(?<=clocked\ by)\ *([^\ ]*)", line).group().strip()
        except AttributeError:
            try:
                return re.search("(?<=clock\ source)\ *\'([^\ ^\']*)", line).group().strip()
            except AttributeError:
                try:
                    return re.search("(?<=clock)\ *([^\ ]*)", line).group().strip()
                except AttributeError:
                    self.sourceclk = ""
        
    def __str__(self):
        return "\n".join(self.lines)
    
    def __repr__(self):
        return self.__str__()
    
    def __dict__(self):
        return {
            "source": self.source,
            "sourceclk": self.sourceclk,
            "destination": self.destination,
            "destinationclk": self.destinationclk,
            "slackns": self.slackns
        }

    
def getSummary(lines):
    for i, line in enumerate(lines):
        if "Design Timing Summary" in line:
            return "\n".join(lines[i:i+8])


def getFailingPaths(lines):
    paths = []
    for i, line in enumerate(lines):
        if "Slack (VIOLATED)" in line:
            j = i
            while(j < len(lines)):
                if lines[j] == "":
                    break
                else:
                    j += 1
            path = TimingPath(lines[i:j])
            paths.append(path.__dict__())
    return pd.DataFrame(paths)


def main():
    lines = [line.strip() for line in open(sys.argv[1])]
    print(getSummary(lines))
    paths = getFailingPaths(lines)
    print(paths)
    paths.to_csv("{}/ci/builds/artifacts/failing_paths.csv".format(os.getenv("CI_PROJECT_DIR")), index=False)


if __name__ == "__main__":
    main()
