import yaml
import sys
import os


def patchConfig(sources, source: str, variable: str) -> None:
    if source in sources and os.getenv(variable) is not None:
        config = sources[source]
        print(f"Original config: {config}")
        if "branch" in config:
            config["branch"] = os.getenv(variable)
        elif "tag" in config:
            config.pop("tag")
            config["branch"] = os.getenv(variable)
        elif "commit" in config:
            config.pop("commit")
            config["branch"] = os.getenv(variable)
        print(f"New config: {config}")


def main() -> None:
    with open(sys.argv[1]) as f:
        config = yaml.safe_load(f)
    sources = config["sources"]
    patchConfig(sources, "dtc", "DTC_BRANCH")
    patchConfig(sources, "mprocessor", "MPROCESSOR_BRANCH")
    with open(sys.argv[1], "w") as f:
        yaml.safe_dump(config, f, default_flow_style=False)



if __name__ == "__main__":
    main()