#!/usr/bin/env bash

var=(*.tgz)
ARCHIVE=${var[0]}
echo $ARCHIVE
BASENAME=$(basename $ARCHIVE .tgz)
gunzip $ARCHIVE
tar rvf $BASENAME.tar $1
gzip $BASENAME.tar
mv $BASENAME.tar.gz $ARCHIVE
