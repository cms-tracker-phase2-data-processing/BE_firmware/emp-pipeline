stages:
  - Generate Files
  - Static Analysis
  - CMSSW Emulation
  - Simulation
  - Make Project
  - Pre-build Checks
  - Synthesis
  - Implementation
  - Package
  - Upload to Registry
  - Test on Hardware

variables:
  IPBB_OS: "c8"
  IPBB_VER: "dev-2021i-v1.3"
  VIVADO_VER: "2020.2"
  PROJECT_DEPFILE: "top.dep"
  PROJECT_NAME: "$CI_PROJECT_NAME-$PROJECT_ALGORITHM"
  BUILD_REPO: $CI_PROJECT_NAME
  EMPHUB_TAG: "latest"

.vivado-job-base:
  image: gitlab-registry.cern.ch/dmonk-emp-projects/docker-images/ipbb-$IPBB_OS:$IPBB_VER
  rules:
    - when: on_success

.vivado-job-build:
  extends: .vivado-job-base
  tags:
    - xilinx-tools
    - c8
    - 64GB
  after_script:
    - mkdir -p $CI_PROJECT_DIR/ci/builds/artifacts
    - rsync -ar $CI_PROJECT_DIR/ci/builds/work/* $CI_PROJECT_DIR/ci/builds/artifacts/ --exclude $CI_PROJECT_DIR/ci/builds/work/var --exclude='*.tar.gz'
    - mc alias set openstack https://$ARTIFACTS_HOST $ARTIFACTS_ACCESS_KEY $ARTIFACTS_SECRET_KEY
    - tar czf $CI_PIPELINE_ID-$(echo "$CI_JOB_NAME" | tr " " _).tar.gz ./ci/builds/artifacts/{src,proj}
    - mc cp $CI_PIPELINE_ID-$(echo "$CI_JOB_NAME" | tr " " _).tar.gz openstack/$ARTIFACTS_BUCKET/

.vivado-job:
  extends: .vivado-job-build
  before_script:
    - mc alias set openstack https://$ARTIFACTS_HOST $ARTIFACTS_ACCESS_KEY $ARTIFACTS_SECRET_KEY
    - mc cp openstack/$ARTIFACTS_BUCKET/$CI_PIPELINE_ID-$(echo "$PREVIOUS_JOB" | tr " " _).tar.gz ./
    - tar xf $CI_PIPELINE_ID-$(echo "$PREVIOUS_JOB" | tr " " _).tar.gz
    - rm -rf $CI_PIPELINE_ID-$(echo "$PREVIOUS_JOB" | tr " " _).tar.gz
    - source /opt/Xilinx/Vivado/$VIVADO_VER/settings64.sh
    - mkdir -p ci/builds/work
    - cd ci/builds/work
    - mkdir proj
    - cp -r $CI_PROJECT_DIR/ci/builds/artifacts/proj/$PROJECT_NAME $CI_PROJECT_DIR/ci/builds/work/proj/
    - cp -r $CI_PROJECT_DIR/ci/builds/artifacts/src $CI_PROJECT_DIR/ci/builds/work/
    # - rsync -ar $CI_PROJECT_DIR $CI_PROJECT_DIR/ci/builds/work/src/ --exclude ci
    - cd proj/$PROJECT_NAME
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
        - ci/builds/work/var
        - ci/builds/work/.ipbb_work.yml

Check Dependencies:
  stage: Static Analysis
  extends: .vivado-job-base
  before_script:
    - mkdir -p ci/builds
    - python3 /ci-tools/setup_project.py $CI_PROJECT_DIR/dependencies.yml
  script:
    - cd ci/builds/work
    - ipbb proj create vivado test $BUILD_REPO:$PROJECT_ALGORITHM $PROJECT_DEPFILE
    - cd proj/test
    - ipbb ipbus gendecoders -f
    - if [[ "$(ipbb dep report | grep errors | wc -l)" -gt 0 ]]; then ipbb dep report; exit 1; else ipbb dep report; fi

Make Project:
  stage: Make Project
  extends: .vivado-job-build
  variables:
    KUBERNETES_MEMORY_REQUEST: 4Gi
    KUBERNETES_CPU_REQUEST: 1
  before_script:
    - mkdir -p ci/builds
    - source /opt/Xilinx/Vivado/$VIVADO_VER/settings64.sh
    - python3 /ci-tools/setup_project.py $CI_PROJECT_DIR/dependencies.yml
  script:
    - cd ci/builds/work
    - ipbb proj create vivado $PROJECT_NAME $BUILD_REPO:$PROJECT_ALGORITHM $PROJECT_DEPFILE
    - cd proj/$PROJECT_NAME
    - ipbb ipbus gendecoders -f
    - ipbb vivado project --enable-ip-cache --single
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
        - ci/builds/work/var
        - ci/builds/work/.ipbb_work.yml

Check Syntax:
  extends: .vivado-job
  stage: Pre-build Checks
  variables:
    KUBERNETES_MEMORY_REQUEST: 4Gi
    KUBERNETES_CPU_REQUEST: 1
    PREVIOUS_JOB: Make Project
  script:
    - echo "Skipping"
#    - ipbb vivado check-syntax

Synthesise:
  extends: .vivado-job
  stage: Synthesis
  variables:
    KUBERNETES_MEMORY_REQUEST: 16Gi
    KUBERNETES_CPU_REQUEST: 4
    PREVIOUS_JOB: Check Syntax
  script:
    - ipbb vivado synth -j$(nproc)
  

Implement:
  extends: .vivado-job
  stage: Implementation
  variables:
    KUBERNETES_MEMORY_REQUEST: 16Gi
    KUBERNETES_CPU_REQUEST: 4
    PREVIOUS_JOB: Synthesise
  script:
    - ipbb vivado impl -j$(nproc)
    - ipbb vivado resource-usage | tee usage.txt
    - pip3 install anybadge
    - mkdir -p $CI_PROJECT_DIR/ci/badges
    - python3 /ci-tools/generate_badges.py usage.txt $CI_PROJECT_DIR/ci/badges
  after_script:
    - mkdir -p $CI_PROJECT_DIR/ci/builds/artifacts
    - rsync -r $CI_PROJECT_DIR/ci/builds/work/* $CI_PROJECT_DIR/ci/builds/artifacts/ --exclude $CI_PROJECT_DIR/ci/builds/work/var
    - python3 /ci-tools/parse_timing_report.py $CI_PROJECT_DIR/ci/builds/artifacts/proj/$PROJECT_NAME/$PROJECT_NAME/$PROJECT_NAME.runs/impl_1/top_timing_summary_routed.rpt
    - mc alias set openstack https://$ARTIFACTS_HOST $ARTIFACTS_ACCESS_KEY $ARTIFACTS_SECRET_KEY
    - tar czf $CI_PIPELINE_ID-$(echo "$CI_JOB_NAME" | tr " " _).tar.gz ./ci/builds/artifacts/{src,proj}
    - mc cp $CI_PIPELINE_ID-$(echo "$CI_JOB_NAME" | tr " " _).tar.gz openstack/$ARTIFACTS_BUCKET/ --recursive
  artifacts:
    when: always
    paths:
      - ci/badges
      - ci/builds/artifacts/failing_paths.csv

Generate Package:
  extends: .vivado-job
  stage: Package
  variables:
    KUBERNETES_MEMORY_REQUEST: 12Gi
    KUBERNETES_CPU_REQUEST: 2
    PREVIOUS_JOB: Implement
  script:
    - ipbb vivado package
    - cd $CI_PROJECT_DIR/ci/builds/work/proj/$PROJECT_NAME/package
    - curl -o connections.xml https://s3.cern.ch/swift/v1/ci-configs/connections.xml
    - bash /ci-tools/append_to_package.sh connections.xml
  artifacts:
    paths:
      - ci/builds/artifacts/proj/$PROJECT_NAME/package

Upload Package to emphub:
    stage: Upload to Registry
    image: registry.cern.ch/docker.io/library/python
    rules:
      - when: on_success
    before_script:
        - python3 -m pip install click minio pyyaml tabulate
        - python3 -m pip install --index-url https://test.pypi.org/simple/ emphub-cli==0.0.3
        - mkdir -p ~/.emp
        - mv $CI_PROJECT_DIR.tmp/emphub_config_file ~/.emp/config.yaml
    script:
        - emp push $PROJECT_NAME:$EMPHUB_TAG --path "$(set -- ci/builds/artifacts/proj/$PROJECT_NAME/package/*.tgz; echo "$1")"
