[![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/commits/master)
---
[![FFs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/FFs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![LUTRAMs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/LUTRAMs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![DSP Blocks](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/DSP%20Blocks.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![Logic LUTs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/Logic%20LUTs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![RAMB18](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/RAMB18.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![RAMB36](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/RAMB36.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![SRLs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/SRLs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![Total LUTs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/Total%20LUTs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)
[![URAM](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/-/jobs/artifacts/master/raw/ci/badges/URAM.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/emp-pipeline/)


# emp-pipeline

Example project which implements the CI pipeline for EMP projects.
