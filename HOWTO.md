# Setting up the CI
In order to start using the integrated EMP CI, a few additions are required to the standard project: a `dependencies.yml` file and a `.gitlab-ci.yml` file.

The `dependencies.yml` file should include a list of all other EMP projects required to build the project, for example `emp-fwk` and `ipbus-firmware`. These are specified as a dictionary in the YAML file, with the key being the name of the project and the value being a dictionary describing the metadata for the project. This metadata must include the project URL, as well as one of either a branch, tag or commit from the repository. HTTP sources should be specified without the port number (see examples below). In order to use a repository, the user must already have access to that respository. Authentication is handled within the CI pipeline. A few examples can be seen in the snippet below
```yaml
---
sources:
    emp-fwk:
        branch: k_fe_mgt
        url: https://gitlab.cern.ch/p2-xware/firmware/emp-fwk.git

    ttc_legacy:
        tag: v2.1
        url: https://gitlab.cern.ch/ttc/legacy_ttc.git

    gbt-fpga:
        tag: gbt_fpga_6_1_0
        url: https://gitlab.cern.ch/gbt-fpga/gbt-fpga.git

    ipbus:
        tag: v1.8
        url: https://github.com/ipbus/ipbus-firmware
    
    tcds2:
        tag: v0_1_1
        url: https://gitlab.cern.ch/cms-tcds/cms-tcds2-firmware.git

    tclink:
        commit: fda0bcf
        url: https://gitlab.cern.ch/HPTD/tclink.git

```

In order to maintain readability and maintainability of the pipeline, the `.gitlab-ci.yml` file makes use of a pre-built template. This means that the user only has to specify the variables specific to the project, rather than the full pipeline structure. This is done by adding the following snippet to the top of the `.gitlab-ci.yml` file:
```yaml
include:
  - project: "dmonk-emp-projects/emp-pipeline"
    file: "/ci/templates/vivado-jobs.yml"
```
The global variables are then set to customise the pipeline for the project. Most variables have default values set within the template, however these may not be appropriate for a given a project. The values of these default variables can be found [here](https://gitlab.cern.ch/dmonk-emp-projects/emp-pipeline/-/blob/master/ci/templates/vivado-jobs.yml). A table of common variables can be found below:
| Variable | Description | Default |
| ---      | ---         | ---     |
| `VIVADO_VER` | Version of Vivado to use to build project. | `"2020.2"` |
| `PROJECT_DEPFILE` | Location of the dep file within the algorithm specified with `PROJECT_ALGORITHM`. | `top.dep` |
| `PROJECT_ALGORITHM` | Algorithm used for top level build. | None. MUST BE SPECIFIED IN EACH PROJECT |
| `PROJECT_NAME` | Name of project created by ipbb. | `$CI_PROJECT_NAME-$PROJECT_ALGORITHM` |
| `EMPHUB_TAG` | Tag for package when it is pushed to the EMPhub registry | `latest` (for a unique tag name, a good choice is `$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA`)

An example snipped to set these variables can be found below:
```yaml
variables:
  VIVADO_VER: "2019.2"
  PROJECT_DEPFILE: top.dep
  PROJECT_ALGORITHM: top
  PROJECT_NAME: muon-e
  EMPHUB_TAG: $CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA
```
With the inclusion of the two snippets described above, any commits to the repository will now trigger a full build pipeline.

# Advanced Usage
Any job within the CI pipeline can be modified or overwritten by adding values to the local `.gitlab-ci.yml` file.

## Modifying Upstream Files
In some cases, it may be necessary to make modifications to one or more files in other repositories. This can be achieved by adding the full filepath to the modified file under a `replacements` directory within the repository root. The following snippet must then be added to the `.gitlab-ci.yml` file to enable these replacements:
```yaml
Make Project:
  before_script:
    - mkdir -p ci/builds
    - source /opt/Xilinx/Vivado/$VIVADO_VER/settings64.sh
    - python3 /ci-tools/setup_project.py $CI_PROJECT_DIR/dependencies.yml
    - cp -r $CI_PROJECT_DIR/replacements/* $CI_PROJECT_DIR/ci/builds/work/src/
```
The script is the same as the default, but with an extra final line to add the replacement files to the correct locations within the IPBB project.
